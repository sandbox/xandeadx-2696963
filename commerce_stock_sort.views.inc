<?php

/**
 * Implements hook_views_data_alter().
 */
function commerce_stock_sort_views_data_alter(&$data) {
  foreach ($data as $table_name => $table_info) {
    foreach ($table_info as $field_name => $field_info) {
      if ($field_name == 'commerce_stock') {
        unset($field_info['argument']);
        unset($field_info['filter']);
        $field_info['title'] = 'In Stock';
        $field_info['title short'] = 'In Stock';
        $field_info['sort']['handler'] = 'views_handler_sort_in_stock';
        $data[$table_name][$field_name . '_in_stock'] = $field_info;
      }
    }
  }
}
