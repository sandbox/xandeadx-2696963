<?php

class views_handler_sort_in_stock extends views_handler_sort {
  function query() {
    $table_alias = $this->ensure_my_table();
    $this->query->add_orderby(NULL, "({$table_alias}.commerce_stock_value > 0)", $this->options['order'], $this->options['id']);
  }
}
